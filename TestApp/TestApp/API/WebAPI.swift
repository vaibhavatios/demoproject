//
//  WebAPI.swift
//  APEditor
//
//  Created by Pinaal Prajapati on 19/08/19.
//  Copyright © 2019 Pinaal Prajapati. All rights reserved.
//


import Foundation
import Alamofire
import UIKit
import SwiftyJSON

struct FileArray {
    var fileURL : URL? = nil
    var withName : String? = nil
    var fileName : String? = nil
    var mimeType : String? = nil
    var data : Data? = nil
}

enum WebResult :String{
    case success
    case error
}

enum WebHeaderKey :String {
    
    case Auth_Key = "Auth-Key"
    case Client_Service = "Client-Service"
    case Content_Type = "Content-Type"
    case userID = "userID"
    case Authorization = "Authorization"
    
}

enum WebApiKey :String {
    
    static let BaseURL = "http://192.168.1.111:8888/FolderName/"
    
    case SignIn = "User/SignIn.php"
    case SignUP = "User/Create.php"
    case Profile = "User/Profile.php"
    
    var relative: String {
        return "\(WebApiKey.BaseURL)\(self.value)"
    }
    
    private var value: String {
        return self.rawValue
    }
    
}

class WebAPI :NSObject {
    
    var backgroundTask : UIBackgroundTaskIdentifier? = nil
    
    internal var dataRequest: DataRequest!
    fileprivate let sessionManager = AF.session
    fileprivate var responseHandlerBlock = ((WebResult) -> Void).self
    
    private func getHeader(_ url: WebApiKey) -> HTTPHeaders {
        
        let xapi = "simplerestapi"
        let contenttype = "application/x-www-form-urlencoded"
        let clientservice = "frontend-client"
        
        switch url {
        
        case .SignUP, .SignIn :
            let header: HTTPHeaders = [
                
                WebHeaderKey.Content_Type.rawValue: contenttype,
                WebHeaderKey.Client_Service.rawValue: clientservice,
                WebHeaderKey.Auth_Key.rawValue :xapi
                
            ]
            
            return header
            
            
        case .Profile:
            let header: HTTPHeaders = [
                
//                WebHeaderKey.userID.rawValue: AllParam.session.integer(forKey: AllParam.sessionKey.userID.value).string ,
//                WebHeaderKey.Authorization.rawValue: AllParam.session.string(forKey: AllParam.sessionKey.token.rawValue) ?? "",
                WebHeaderKey.Content_Type.rawValue: contenttype,
                WebHeaderKey.Client_Service.rawValue: clientservice,
                WebHeaderKey.Auth_Key.rawValue :xapi
                
            ]
            
            return header
            
        }
        
    }
    
    @discardableResult
    init(postRequest url:WebApiKey,parameters :Parameters?,requesttimeout : TimeInterval = 90,completionHandler : @escaping (Result<Any,Error>) -> Void) {
        super.init()
        
        let header = self.getHeader(url)
        
        Debug.Log("\n\n===========Request===========")
        Debug.Log("Url: " + url.relative)
        Debug.Log("Method: POST")
        Debug.Log("Header: " + header.description)
        Debug.Log("Parameter: " + (parameters?.description ?? ""))
        Debug.Log("=============================\n")
        
        sessionManager.configuration.timeoutIntervalForRequest = requesttimeout
        
        DispatchQueue.global(qos: .background).async {
            AF.request(url.relative, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { res in
                
                Debug.Log("\n\n===========Response===========")
                Debug.Log("Url: " + url.relative)
                Debug.Log("Method: POST")
                Debug.Log("Header: " + header.description)
                Debug.Log("Parameter: " + (parameters?.description ?? ""))
                Debug.Log("Response: " + (res.data != nil ? String.init(data: res.data!, encoding: .utf8) ?? "No Data" : "No DATA"))
                Debug.Log("=============================\n")
                
                switch res.result {
                
                case .success(let value):
                    completionHandler(.success(value))
                    break
                    
                case .failure(let error):
                    completionHandler(.failure(error))
                    break
                    
                }
                
            })
            
        }
        
    }
    
    @discardableResult
    init(getRequest url:WebApiKey,parameters :Parameters? = nil,requesttimeout : TimeInterval = 90,completionHandler : @escaping (Result<Any,Error>) -> Void) {
        super.init()
        
        let header = self.getHeader(url)
        
        Debug.Log("\n\n===========Request===========")
        Debug.Log("Url: " + url.relative)
        Debug.Log("Method: GET")
        Debug.Log("Header: " + header.description)
        Debug.Log("Parameter: " + (parameters?.description ?? ""))
        Debug.Log("=============================\n")
        
        sessionManager.configuration.timeoutIntervalForRequest = requesttimeout
        
        DispatchQueue.global(qos: .background).async {
            AF.request(url.relative, method: .get, parameters: parameters, headers: header).responseJSON { res in
                Debug.Log("\n\n===========Response===========")
                Debug.Log("Url: " + url.relative)
                Debug.Log("Method: GET")
                Debug.Log("Header: " + header.description)
                Debug.Log("Parameter: " + (parameters?.description ?? ""))
                Debug.Log("Response: " + (res.data != nil ? String.init(data: res.data!, encoding: .utf8) ?? "No Data" : "No DATA"))
                Debug.Log("=============================\n")
                
                switch res.result {
                
                case .success(let value):
                    completionHandler(.success(value))
                    break
                    
                case .failure(let error):
                    completionHandler(.failure(error))
                    break
                    
                }
        
            }
            
        }
        
    }
    
    @discardableResult
    init(uploadRequest url:WebApiKey,parameters :Parameters, fileArray : [FileArray]?,requesttimeout : TimeInterval = 90,progressHandler: ((_ progress:Double) -> Void)?,completionHandler : @escaping (Result<Any,Error>) -> Void) {
        
        super.init()
        
        let header = self.getHeader(url)
        
        Debug.Log("\n\n===========Request===========")
        Debug.Log("Url: " + url.relative)
        Debug.Log("Method: POST")
        Debug.Log("Header: " + header.description)
        Debug.Log("Parameter: " + parameters.description)
        Debug.Log("=============================\n")
        
        sessionManager.configuration.timeoutIntervalForRequest = requesttimeout
        
        startBackgroundTask()
        
        DispatchQueue.global(qos: .background).async {
            
            AF.upload(multipartFormData: {  multipartdata in
                if fileArray != nil{
                    if fileArray!.count > 0{
                        fileArray?.forEach({
                            if $0.fileURL != nil && $0.withName != nil && $0.fileName != nil && $0.mimeType != nil{
                                multipartdata.append($0.fileURL!, withName: "\($0.withName!)[]", fileName: $0.fileName!, mimeType: $0.mimeType!)
                            }
                            else  if $0.data != nil && $0.withName != nil && $0.fileName != nil && $0.mimeType != nil{
                                multipartdata.append($0.data!, withName: "\($0.withName!)[]", fileName: $0.fileName!, mimeType: $0.mimeType!)
                            }
                        })
                    }
                }
                
                parameters.forEach({
                    multipartdata.append(($0.value as! String).data(using: .utf8)!, withName: $0.key)
                })
            }, to: url.relative, method: .post, headers: header)
            .uploadProgress(closure: { progress in
                progressHandler?(progress.fractionCompleted)
            })
            .response { res in
                
                switch res.result{
                case .success(let data):
                    Debug.Log("\n\n===========Response success===========")
                    Debug.Log("Url: " + url.relative)
                    Debug.Log("Method: POST")
                    Debug.Log("Header: " + header.description)
                    Debug.Log("Parameter: " + parameters.description)
                    Debug.Log("Response: " + (data != nil ? String.init(data: data!, encoding: .utf8) ?? "No Data" : "No DATA"))
                    Debug.Log("=============================\n")
                    
                    self.endBackgroundTask()
                    if data != nil {
                        completionHandler(.success(data!))
                    }
                    else{
                        completionHandler(.failure(NSError.init(domain: "Upload Error", code: 401, userInfo: nil)))
                    }
                    
                    break
                case .failure(let error):
                    Debug.Log("\n\n===========Response failure===========")
                    Debug.Log("Url: " + url.relative)
                    Debug.Log("Method: POST")
                    Debug.Log("Header: " + header.description)
                    Debug.Log("Parameter: " + parameters.description)
                    Debug.Log("Response(failer): " + error.localizedDescription)
                    Debug.Log("=============================\n")
                    
                    self.endBackgroundTask()
                    completionHandler(.failure(error))
                    break
                }
            }
            
        }
        
    }
    
    
    
    func startBackgroundTask(){
        endBackgroundTask()
        backgroundTask = UIApplication.shared.beginBackgroundTask()
    }
    func endBackgroundTask(){
        if backgroundTask != nil{
            UIApplication.shared.endBackgroundTask(backgroundTask!)
        }
        backgroundTask = UIBackgroundTaskIdentifier.invalid
        backgroundTask = nil
    }
    
    deinit {
        dataRequest = nil
        backgroundTask = UIBackgroundTaskIdentifier.invalid
        backgroundTask = nil
    }
    
}
