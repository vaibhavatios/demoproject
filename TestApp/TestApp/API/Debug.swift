//
//  Debug.swift
//  APEditor
//
//  Created by Pinaal Prajapati on 22/08/19.
//  Copyright © 2019 Pinaal Prajapati. All rights reserved.
//

import Foundation

struct Debug{
    static func Log(_ msg :String){
        #if DEBUG
        print(msg)
        #endif
        
    }
    static func DebugPrint(_ error : Any){
        #if DEBUG
        debugPrint(error)
        #endif
    }
}
